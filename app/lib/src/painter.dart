import 'dart:ui';

import 'package:flutter/material.dart';

class PaintData {
  final Paint paint;

  PaintData(this.paint);
}

class PencilPaintData extends PaintData {
  final Path path;

  PencilPaintData(this.path, Paint paint) : super(paint);
}

class LinePaintData extends PaintData {
  final Offset startPoint;
  Offset endPoint;

  LinePaintData(this.startPoint, this.endPoint, Paint paint) : super(paint);
}

class RectanglePaintData extends PaintData {
  final Offset startPoint;
  Offset endPoint;

  RectanglePaintData(this.startPoint, this.endPoint, Paint paint) : super(paint);
}

class MainPainter extends CustomPainter {
  final List<PaintData> paintData;

  MainPainter(this.paintData);

  @override
  void paint(Canvas canvas, Size size) {
    for (var data in paintData) {
      if (data is PencilPaintData) {
        canvas.drawPath(data.path, data.paint);
      } else if (data is LinePaintData) {
        canvas.drawLine(data.startPoint, data.endPoint, data.paint);
      } else if (data is RectanglePaintData) {
        canvas.drawRect(Rect.fromPoints(data.startPoint, data.endPoint), data.paint);
      }
    }
  }

  @override
  bool shouldRepaint(covariant MainPainter oldDelegate) => true;
}
