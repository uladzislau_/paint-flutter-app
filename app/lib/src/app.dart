import 'package:app/src/painter.dart';
import 'package:app/src/tool_panel.dart';
import 'package:flutter/material.dart';

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          body: MainAppPage(),
        ),
      ),
    );
  }
}

class MainAppPage extends StatefulWidget {
  @override
  _MainAppPageState createState() => _MainAppPageState();
}

class _MainAppPageState extends State<MainAppPage> {
  final List<PaintData> paintDataList = [];
  late PaintData curPaintData;
  Color curColor = Colors.black;
  double curWidth = 4;
  PaintTool curPaintTool = PaintTool.pencil;
  final List<Offset?> points = [];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanStart: (details) {
        var paint = Paint()
          ..color = (curPaintTool == PaintTool.eraser) ? Colors.white : curColor
          ..strokeCap = StrokeCap.round
          ..style = PaintingStyle.stroke
          ..strokeWidth = curWidth;

        var renderBox = context.findRenderObject() as RenderBox;
        var offset = renderBox.globalToLocal(details.globalPosition);

        if (curPaintTool == PaintTool.pencil || curPaintTool == PaintTool.eraser) {
          curPaintData = PencilPaintData(Path()..moveTo(offset.dx, offset.dy), paint);
        } else if (curPaintTool == PaintTool.line) {
          curPaintData = LinePaintData(offset, offset, paint);
        } else if (curPaintTool == PaintTool.rectangle) {
          curPaintData = RectanglePaintData(offset, offset, paint);
        }

        setState(() {
          paintDataList.add(curPaintData);
        });
      },
      onPanUpdate: (details) {
        var renderBox = context.findRenderObject() as RenderBox;
        var offset = renderBox.globalToLocal(details.globalPosition);
        var data = curPaintData;

        setState(() {
          if (data is PencilPaintData) {
            data.path.lineTo(offset.dx, offset.dy);
          } else if (data is LinePaintData) {
            data.endPoint = offset;
          } else if (data is RectanglePaintData) {
            data.endPoint = offset;
          }
        });
      },
      child: Column(
        children: [
          // Canvas
          Expanded(
            child: CustomPaint(
              size: Size.infinite,
              painter: MainPainter(paintDataList),
            ),
          ),
          // Tool panel
          ToolPanel(
            onToolSelected: (selectedTool) {
              switch (selectedTool) {
                case PaintTool.clear:
                  setState(() => paintDataList.clear());
                  break;
                case PaintTool.eraser:
                  setState(() => curPaintTool = PaintTool.eraser);
                  break;
                case PaintTool.pencil:
                  setState(() => curPaintTool = PaintTool.pencil);
                  break;
                case PaintTool.line:
                  setState(() => curPaintTool = PaintTool.line);
                  break;
                case PaintTool.rectangle:
                  setState(() => curPaintTool = PaintTool.rectangle);
                  break;
              }
            },
            onColorSelected: (color) => setState(() => curColor = color),
            onWidthSelected: (width) => setState(() => curWidth = width),
          ),
        ],
      ),
    );
  }
}
