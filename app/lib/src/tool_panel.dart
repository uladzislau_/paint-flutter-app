import 'package:app/src/dialogs.dart';
import 'package:flutter/material.dart';

enum PaintTool {
  pencil,
  eraser,
  line,
  rectangle,
  clear,
}

class ToolPanel extends StatefulWidget {
  final void Function(PaintTool) onToolSelected;
  final void Function(Color) onColorSelected;
  final void Function(double) onWidthSelected;

  ToolPanel({
    required this.onToolSelected,
    required this.onColorSelected,
    required this.onWidthSelected,
  });

  @override
  _ToolPanelState createState() => _ToolPanelState();
}

class _ToolPanelState extends State<ToolPanel> {
  PaintTool selectedTool = PaintTool.pencil;
  Color curColor = Colors.black;
  double curWidth = 4;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        color: Colors.indigo.shade50,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(8)),
      ),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          SizedBox(width: 16),
          // Color
          ShapeWidget(
            curColor,
            () => showDialog(
              context: context,
              builder: (context) => ColorPickerDialog(
                onColorSelected: (color) {
                  setState(() => curColor = color);
                  widget.onColorSelected(color);
                },
              ),
            ),
            32,
            BoxShape.rectangle,
          ),
          SizedBox(width: 16),
          // Width
          ShapeWidget(
            curColor,
            () => showDialog(
              context: context,
              builder: (context) => WidthPickerDialog(
                color: curColor,
                onWidthSelected: (width) {
                  setState(() => curWidth = width);
                  widget.onWidthSelected(width);
                },
              ),
            ),
            curWidth,
            BoxShape.circle,
          ),
          SizedBox(width: 16),
          // Pencil
          ToolWidget(
            PaintTool.pencil,
            selectedTool,
            () {
              setState(() => selectedTool = PaintTool.pencil);
              widget.onToolSelected(PaintTool.pencil);
            },
          ),
          SizedBox(width: 16),
          // Eraser
          ToolWidget(
            PaintTool.eraser,
            selectedTool,
            () {
              setState(() => selectedTool = PaintTool.eraser);
              widget.onToolSelected(PaintTool.eraser);
            },
          ),
          SizedBox(width: 16),
          // Line
          ToolWidget(
            PaintTool.line,
            selectedTool,
            () {
              setState(() => selectedTool = PaintTool.line);
              widget.onToolSelected(PaintTool.line);
            },
          ),
          SizedBox(width: 16),
          // Rectangle
          ToolWidget(
            PaintTool.rectangle,
            selectedTool,
            () {
              setState(() => selectedTool = PaintTool.rectangle);
              widget.onToolSelected(PaintTool.rectangle);
            },
          ),
          SizedBox(width: 16),
          // Clear screen
          ToolWidget(
            PaintTool.clear,
            selectedTool,
            () {
              widget.onToolSelected(PaintTool.clear);
            },
          ),
          SizedBox(width: 16),
        ],
      ),
    );
  }
}

class ToolWidget extends StatelessWidget {
  final PaintTool tool;
  final PaintTool selectedTool;
  final void Function() onSelected;

  ToolWidget(this.tool, this.selectedTool, this.onSelected);

  @override
  Widget build(BuildContext context) {
    late IconData icon;
    switch (tool) {
      case PaintTool.pencil:
        icon = Icons.edit;
        break;
      case PaintTool.eraser:
        icon = Icons.cleaning_services;
        break;
      case PaintTool.line:
        icon = Icons.horizontal_rule;
        break;
      case PaintTool.rectangle:
        icon = Icons.check_box_outline_blank;
        break;
      case PaintTool.clear:
        icon = Icons.clear;
        break;
    }

    return GestureDetector(
      onTap: onSelected,
      behavior: HitTestBehavior.translucent,
      child: Icon(icon, color: (tool == selectedTool) ? Colors.red : Colors.indigo, size: 32),
    );
  }
}

class ShapeWidget extends StatelessWidget {
  final Color color;
  final void Function() onSelected;
  final double size;
  final BoxShape shape;

  ShapeWidget(this.color, this.onSelected, this.size, this.shape);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onSelected,
      behavior: HitTestBehavior.translucent,
      child: Container(
        width: 32,
        height: 32,
        child: Center(
          child: Container(
            width: size,
            height: size,
            decoration: BoxDecoration(
              color: color,
              shape: shape,
            ),
          ),
        ),
      ),
    );
  }
}
