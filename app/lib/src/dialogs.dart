import 'package:app/src/tool_panel.dart';
import 'package:flutter/material.dart';

var _colors = [
  Colors.black,
  Colors.yellow,
  Colors.lime,
  Colors.amber,
  Colors.red,
  Colors.pink,
  Colors.green,
  Colors.teal,
  Colors.cyan,
  Colors.blue,
  Colors.indigo,
  Colors.purple,
  Colors.brown,
];

class ColorPickerDialog extends StatelessWidget {
  final void Function(Color) onColorSelected;

  ColorPickerDialog({
    required this.onColorSelected,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 300,
        height: 300,
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
        ),
        child: Column(
          children: [
            Expanded(
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 48,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                ),
                itemCount: _colors.length,
                itemBuilder: (context, i) => GestureDetector(
                    child: ColorWidget(_colors[i]),
                    onTap: () {
                      onColorSelected(_colors[i]);
                      Navigator.of(context).pop();
                    }),
              ),
            ),
            CancelButton(() => Navigator.of(context).pop()),
          ],
        ),
      ),
    );
  }
}

List<double> _width = [2, 4, 6, 8, 10, 12, 14, 16, 20, 24, 28, 32];

class WidthPickerDialog extends StatelessWidget {
  final Color color;
  final void Function(double) onWidthSelected;

  WidthPickerDialog({
    required this.color,
    required this.onWidthSelected,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 300,
        height: 300,
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
        ),
        child: Column(
          children: [
            Expanded(
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 48,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                ),
                itemCount: _width.length,
                itemBuilder: (context, i) => ShapeWidget(
                  color,
                  () {
                    onWidthSelected(_width[i]);
                    Navigator.of(context).pop();
                  },
                  _width[i],
                  BoxShape.circle,
                ),
              ),
            ),
            CancelButton(() => Navigator.of(context).pop()),
          ],
        ),
      ),
    );
  }
}

class ColorWidget extends StatelessWidget {
  final Color color;

  ColorWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
    );
  }
}

class CancelButton extends StatelessWidget {
  final void Function() onPressed;

  CancelButton(this.onPressed);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      behavior: HitTestBehavior.translucent,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(color: Colors.indigo.shade100, borderRadius: BorderRadius.circular(16)),
        child: Text('Cancel', style: TextStyle(fontSize: 20), textAlign: TextAlign.center),
      ),
    );
  }
}
